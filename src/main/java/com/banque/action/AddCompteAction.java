package com.banque.action;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.dao.CompteDAO;
import com.banque.dao.CompteDAOImpl;
import com.banque.listener.HibernateListener;
import com.banque.model.Compte;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AddCompteAction extends ActionSupport implements ModelDriven<Compte> {
	private Compte compte = new Compte();
	@Action (value = "register-compte",
			results= {@Result(name = "success", location="add-compte.jsp")})
	public String registerCompte() {
		SessionFactory sessionFactory = (SessionFactory) ServletActionContext.getServletContext()
				.getAttribute(HibernateListener.KEY_NANE);
		CompteDAO compteDAO = new CompteDAOImpl(sessionFactory);
		compteDAO.save(compte);
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(compte);
		session.getTransaction().commit();
		return SUCCESS;
	}
	public String execute() throws Exception{
		return SUCCESS;
	}

	@Override
	public Compte getModel() {
		return compte;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	public void validate() {
		if(compte.getSolde() < 100000 ) {
			addFieldError("Solde", "Solde minimum requise 100 000FCFA");
		}
	}


}
