package com.banque.action;

import com.banque.model.Client;
import com.banque.model.Transaction;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.dao.TransactionDAO;
import com.banque.dao.TransactionDAOImpl;
import com.banque.listener.HibernateListener;
import com.banque.model.Transaction;


public class TransactionAction extends ActionSupport implements ModelDriven<Transaction>{
	private Transaction transaction = new Transaction();
	@Action (value = "register-transaction",
			results= {@Result(name = "success", location="transaction.jsp")})
	public String registerTransaction() {
		SessionFactory sessionFactory = (SessionFactory) ServletActionContext.getServletContext()
				.getAttribute(HibernateListener.KEY_NANE);
		TransactionDAO transactionDAO = new TransactionDAOImpl(sessionFactory);
		transactionDAO.save(transaction);
		return SUCCESS;
		//Session session = sessionFactory.openSession();
		//session.beginTransaction();
		//session.save(transaction);
		//session.getTransaction().commit();
		//return SUCCESS;
}
	public String execute() throws Exception{
		return SUCCESS;
}
	
	@Override
	public Transaction getModel() {
		// TODO Auto-generated method stub
		return transaction;
	}
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	public boolean virer(Transaction transaction) {
		if (transaction.getNumeroemetteur().getSolde() >= transaction.getMontant()) {
			transaction.getNumerodestinataire()
					.setSolde(transaction.getNumeroemetteur().getSolde() - transaction.getMontant());
			transaction.getNumerodestinataire()
					.setSolde(transaction.getNumerodestinataire().getSolde() + transaction.getMontant());
			return true;
		} 
		else {
			return false;
		}
	}
	}

	/*public void validate() {
		if(transaction.getLibelle() == null || transaction.getLibelle().isEmpty() ) {
			addFieldError("Libelle", "Le libelle est requis");
		}
		if(transaction.getMontant() == null || transaction.getMontant().isEmpty() ) {
			addFieldError("prenom", "Le prenom est requis");
		}
		if(transaction.getNumerodestinataire().isEmpty())  {
			addFieldError("date de naissance ", "La date de naissance est requise");
		}
		if(transaction.getNumeroEmetteur() == null || transaction.getTelephone().isEmpty() ) {
			addFieldError("Telephone", "Le numero de telephone est requis");
		}*/
		


