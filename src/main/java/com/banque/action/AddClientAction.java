package com.banque.action;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.dao.ClientDAO;
import com.banque.dao.ClientDAOImpl;
import com.banque.listener.HibernateListener;
import com.banque.model.Client;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AddClientAction extends ActionSupport implements ModelDriven<Client> {
	private Client client = new Client();
	
	
	@Action (value = "register-client",
			results= {@Result(name = "success", location="add-client.jsp")})
	public String registerClient() {
		SessionFactory sessionFactory = (SessionFactory) ServletActionContext.getServletContext()
				.getAttribute(HibernateListener.KEY_NANE);
		ClientDAO clientDAO = new ClientDAOImpl(sessionFactory);
		clientDAO.save(client);
		return SUCCESS;
		//Session session = sessionFactory.openSession();
		//session.beginTransaction();
		//session.save(client);
		//session.getTransaction().commit();
		//return SUCCESS;
}
	public String execute() throws Exception{
		return SUCCESS;
}
	
	@Override
	public Client getModel() {
		// TODO Auto-generated method stub
		return client;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}

	/*public void validate() {
		if(client.getNom() == null || client.getNom().isEmpty() ) {
			addFieldError("nom", "Le nom est requis");
		}
		if(client.getPrenom() == null)
				//|| client.getPrenom().isEmpty() ) 
				{
			addFieldError("prenom", "Le prenom est requis");
		}
		if(client.getDatenaissance() == null)  {
			addFieldError("date de naissance ", "La date de naissance est requise");
		}
		if(client.getTelephone() == null || client.getTelephone().isEmpty() ) {
			addFieldError("Telephone", "Le numero de telephone est requis");
		}
		if(client.getEmail() == null || client.getEmail().isEmpty() ) {
			addFieldError("email", "Le mail est requis");
		}
		if(client.getNationalite() == null || client.getNationalite().isEmpty() ) {
			addFieldError("nationalite", "La nationalite est requise");
		}
}*/

}
