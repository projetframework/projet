package com.banque.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

public class HomeAction extends ActionSupport{
	
	@Action (value = "",
			results= {@Result(name = "success", location="index.jsp")})
	public String homePage ( ) {
		return SUCCESS;
	
	}
	

}
