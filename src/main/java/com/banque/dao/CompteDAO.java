package com.banque.dao;

import java.util.List;

import com.banque.model.Compte;

public interface CompteDAO {
	public void save(Compte compte);
	public void remove(Compte compte);
	public List<Compte> findAll();

}
