package com.banque.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.model.Transaction;

public class TransactionDAOImpl implements TransactionDAO {
private SessionFactory sessionFactory;
	
	public  TransactionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public void save(Transaction transaction) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(transaction);
		session.getTransaction().commit();
	}

	@Override
	public void remove(Transaction transaction) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(transaction);
		session.getTransaction().commit();
		
	}

	@Override
	public List<Transaction> findAll() {
		Session session = sessionFactory.openSession();
		List<Transaction> result = session.createQuery("from Transaction").list();
		return result;
	}



}
