package com.banque.dao;

import java.util.List;

import com.banque.model.Client;


public interface ClientDAO {
	public void save(Client client);
	public void remove(Client client);
	public List<Client> findAll();

}
