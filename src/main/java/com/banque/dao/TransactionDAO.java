package com.banque.dao;

import java.util.List;

import com.banque.model.Transaction;

public  interface TransactionDAO {
	public void save(Transaction transaction);
	public void remove(Transaction transaction);
	public List<Transaction> findAll();

}
