package com.banque.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.model.Compte;

public class CompteDAOImpl implements CompteDAO {
private SessionFactory sessionFactory;
	
	public  CompteDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public void save(Compte compte) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(compte);
		session.getTransaction().commit();
	}

	@Override
	public void remove(Compte compte) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(compte);
		session.getTransaction().commit();
		
	}

	@Override
	public List<Compte> findAll() {
		Session session = sessionFactory.openSession();
		List<Compte> result = session.createQuery("from Compte").list();
		return result;
	}

}
