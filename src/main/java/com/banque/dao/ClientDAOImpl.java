package com.banque.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.banque.model.Client;

public class ClientDAOImpl implements ClientDAO {
private SessionFactory sessionFactory;
	
	public  ClientDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public void save(Client client) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(client);
		session.getTransaction().commit();
	}

	@Override
	public void remove(Client client) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(client);
		session.getTransaction().commit();
		
	}

	@Override
	public List<Client> findAll() {
		Session session = sessionFactory.openSession();
		List<Client> result = session.createQuery("from Client").list();
		return result;
	}

}
