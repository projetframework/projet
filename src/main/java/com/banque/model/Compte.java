package com.banque.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name = "Compte")
public class Compte {
	private float numero;
	private float solde;
	private double password;
	@Id
	@GeneratedValue
	private int id;
	@ManyToOne
	private Client client;
	private List<Transaction> transaction = new ArrayList<>( );
public Compte() {
		
		numero = getRandomCompte();
		this.password = getRandomPassword();
	}
	

	public float getNumero() {
		return numero;
	}

	public void setNumero(float numero) {
		this.numero = numero;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public double getPassword() {
		return password;
	}

	public void setPassword(double password) {
		this.password = password;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	public double getRandomPassword()
	{
		double x = ((Math.random()*((9998 - 1000) + 1)) + 1000);
		return x;
	}
	
	public int getRandomCompte()
	{
		int x = (int) ((Math.random()*((999998 - 100000) + 1)) + 100000);
		return x;
	}
	
	
	
	 
}
