package com.banque.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;

@Entity(name="Transaction")
public class Transaction {
	private String libelle;
	@CreationTimestamp
	private Date date;
	private float montant;
	@ManyToOne
	private Compte numerodestinataire;
	@ManyToOne
	private Compte numeroemetteur;
	@Id
	@GeneratedValue
	private int id;
	@OneToMany(mappedBy = "transaction",
	        cascade = CascadeType.ALL )
	    private List<Transaction> transaction = new ArrayList<>( );

	
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float getMontant() {
		return montant;
	}
	public void setMontant(float montant) {
		this.montant = montant;
	}
	public Compte getNumerodestinataire() {
		return numerodestinataire;
	}
	public void setNumerodestinataire(Compte numerodestinataire) {
		this.numerodestinataire = numerodestinataire;
	}
	public Compte getNumeroemetteur() {
		return numeroemetteur;
	}
	public void setNumeroemetteur(Compte  numeroemetteur) {
		this.numeroemetteur = numeroemetteur;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Transaction> getTransaction() {
		return transaction;
	}
	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction;
	}

}
