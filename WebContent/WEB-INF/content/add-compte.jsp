<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Nouveau Compte></title>

</head>
<body>
<h1>  Compte</h1>
			<s:form action="register-compte" method="POST" validate="true">
				<s:textfield label="solde" name="solde"></s:textfield>
				<s:textfield label="numero" name="numero"></s:textfield>
				<s:hidden name="submitted" value="true"></s:hidden>
				<s:hidden name="solde" value="%{solde}"></s:hidden>
				<s:hidden name="numero" value="%{numero}"></s:hidden>
				<s:submit value="Submit" ></s:submit>
				<s:submit value="Transaction" href="transaction"></s:submit>
			</s:form>
</body>
</html>