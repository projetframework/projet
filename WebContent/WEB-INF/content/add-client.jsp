<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1"> 
<title></title>
</head>
<body>
<h1>  Client </h1>
	<s:form action="register-client">
		<s:textfield label="Votre nom" name="nom"></s:textfield>
		<s:textfield label="Votre prenom" name="prenom"></s:textfield>
		<s:textfield label="Votre date de naissance" name="datenaissance"
			type="date"></s:textfield>
		<s:textfield label="Votre numero de telephone" name="telephone"></s:textfield>
		<s:textfield label="votre email" name="email" type="email"></s:textfield>
		<s:textfield label="Votre nationalite" name="nationalite"></s:textfield>
		<s:hidden name="Valider" value ="true"></s:hidden>
		<s:submit value="Valider" href="SoldeInitial"></s:submit>
	</s:form>
</body>
</html>